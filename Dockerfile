FROM alpine:latest

COPY . /

RUN apk add nodejs npm ;\
    (cd /app ; npm install --production);\
    apk del npm

EXPOSE 8080/tcp

CMD [ "/bin/sh", "-c", "(cd app ; node server.js)" ]
