const alternate = { exact: false, words: 'kitten' };
const map = {
  'spider':         alternate,
  'cave cricket':   alternate,
  'centipede':      alternate,
  'insect':         alternate,
  'gazebo':         { words: 'gazebo', url: 'https://nomi-and-tim.org/eleanor/jan-june2016/e-t-dance-game%20-%201.jpg' }
};

module.exports = {

  map: function(lookup) {
    const words = lookup.toLowerCase();
    // Exact
    const alt = map[words] || map[`${words}s`];
    if (alt) {
      return alt;
    }
    // Vague
    for (let match in map) {
      if (!map[match].exact && words.indexOf(match) !== -1) {
        return map[match];
      }
    }
    // No substitution
    return { words: lookup };
  }

};
