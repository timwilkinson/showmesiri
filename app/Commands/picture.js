const fetch = require('node-fetch');
const Log = require('debug')('picture');
const BadWords = require('./badwords');

const idealWidth = 1080;
const idealHeight = 1920;
const idealRatio = idealWidth / idealHeight;
const KEY = process.env.MICROSOFT_SEARCH_KEY;
const HOST = 'cognitivegblprod-westus-01.regional.azure-api.net';
// const HOST = 'api.cognitive.microsoft.com';


const Picture = {

  async run(words) {

    Log('Picture', words);

    if (!words) {
      return {
        tell: `I can't find a pictures of ${words}s.`
      };
    }

    const thing = BadWords.map(words);

    if (thing.url) {
      return {
        tell: `Looking for a picture of ${thing.words}.`,
        title: thing.words,
        imageTimeout: 60,
        imageUrl: thing.url
      };
    }

    const response = await (await fetch(`https://${HOST}/bing/v7.0/images/search?q=${encodeURIComponent(thing.words)}&mkt=en-US&safeSearch=Moderate`, {
      headers: {
        'Ocp-Apim-Subscription-Key': KEY
      }
    })).json();

    if (!response.value.length) {
      return {
        tell: `I couldn't find a picture of ${words}.`
      };
    }

    let best = null;
    for (let i = 0; i < response.value.length; i++) {
      const current = response.value[i];
      const diffRatio = Math.abs(current.width / current.height - idealRatio);
      if (best === null || diffRatio < best.diff) {
        best = { diff: diffRatio, url: current.contentUrl };
      }
    }
    return {
      tell: `Looking for a picture of ${thing.words}.`,
      title: thing.words,
      imageTimeout: 60,
      imageUrl: best.url
    };
  }
}

module.exports = Picture;
