const Log = require('debug')('page');

const Commands = {
  picture: require('./picture')
};

async function CMD(ctx) {
  const cmd = Commands[ctx.request.header.t];
  if (!cmd) {
    return {
      tell: `I'm sorry, I don't know anything about that.`
    };
  }
  const result = await cmd.run(ctx.request.header.q);
  Log(result);

  if (result.imageUrl) {
    BUS.emit('render.image', { url: result.imageUrl, timeout: result.imageTimeout || 60 });
  }

  ctx.body = result.tell || 'Okay';
  ctx.type = 'text/plain';
}


module.exports = function(root, wsroot) {
  root.get('/cmd', CMD);
}
