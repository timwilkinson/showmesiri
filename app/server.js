#! /usr/bin/env node

//
// https://www.electricobjects.com/set_url
//
const Koa = require('koa');
const Websockify = require('koa-websocket');
const CacheControl = require('koa-cache-control');
const Router = require('koa-router');
const EventEmitter = require('events');
const Pages = require('./Pages');
const Commands = require('./Commands');
const Log = require('debug')('app');

const WEBPORT = 8080;

global.BUS = new EventEmitter();

const App = Websockify(new Koa());
App.on('error', err => Log(err));

App.use(CacheControl({ noCache: true }));

const root = Router();
const wsroot = Router();

Pages(root, wsroot);
Commands(root, wsroot);

App.use(root.middleware());
App.ws.use(wsroot.middleware());
App.ws.use(async (ctx, next) => {
  await next(ctx);
  if (ctx.websocket.listenerCount('message') === 0) {
    ctx.websocket.close();
  }
});

App.listen({
  port: WEBPORT
});
