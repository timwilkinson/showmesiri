const FS = require('fs');
const Log = require('debug')('page');

async function HTML(ctx) {
  ctx.body = FS.readFileSync(`${__dirname}/page.html`, { encoding: 'utf8' });
  ctx.type = 'text/html';
}

async function IMG(ctx) {
  ctx.body = FS.readFileSync(`${__dirname}/main.jpg`);
  ctx.type = 'image/jpg';
}

async function WS(ctx) {

  const onMessage = {};

  function send(cmd, value) {
    try {
      ctx.websocket.send(JSON.stringify({
        cmd: cmd,
        value: value
      }));
    }
    catch (_) {
      Log(_);
    }
  }

  function renderImage(args) {
    send('render.image', args);
  }
  BUS.on('render.image', renderImage);

  ctx.websocket.on('close', () => {
    BUS.off('render.image', renderImage);
  });

  ctx.websocket.on('error', () => {
    ctx.websocket.close();
  });

  ctx.websocket.on('message', async data => {
    try {
      const msg = JSON.parse(data);
      let ctx = null;
      let fn = onMessage[msg.cmd];
      if (fn) {
        try {
          Log(msg);
          await fn.call(ctx, msg);
        }
        catch (e) {
          Log(e);
        }
      }
    }
    catch (e) {
      console.error(e);
    }
  });

}

module.exports = function(root, wsroot) {
  root.get('/', HTML);
  root.get('/main.jpg', IMG);
  wsroot.get('/ws', WS);
}
